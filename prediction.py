#This script is used to load the pre-trained weights for the CNN model, randomly pick one of the the test images which were splitted during the training stage, adding to it a uniformly random scalar, predicting the scalar with the trained model and fixing the modified image by substracting the predicted scalar, to finally plot all three images and print the true and predicted scalar values.
from model import BRC
#import cv2
import numpy as np
from tensorflow.keras.utils import load_img, img_to_array, array_to_img 
from tensorflow.image import adjust_brightness, resize
from matplotlib import pyplot as plt
import os 
from random import choice
from directories import *
import argparse

#cat is the category of images to work with
parser_category = argparse.ArgumentParser() 
parser_category.add_argument('-cat', '--category', type = str, required = True, help = "This script used the trained CNN model to automatically fix brightnes for images in the google scrapped images dataset. These images are divided in four categories, please select one of them: 'arch' for architecture, 'a&t' for art and culture, 'fd' for food and drink, 'ta' for travel and andventure") 
args = parser_category.parse_args()

cat = args.category
model = BRC() #get the model
model.model.load_weights(cat + '.h5') #load the pre-trained weights

path = images_path(cat) #get the path to the images dataset
path_test_images = path + 'test_images/' #the path to test images
if not os.path.exists(path_test_images):
    raise ValueError('Please create the folder {} with the images to modify and fix. This is automatically done when training.py is compiled'.format(path_test_images))

path_modified_images = path + 'modified_images/'
create_if_doesnt_exist(path_modified_images) #create a folder for modified images if doesn't exist

path_fixed_images = path + 'fixed_images/'
create_if_doesnt_exist(path_fixed_images) #create a folder for fixed images if doesn't exist


#This block randomly picks a test image
test_files_list = os.listdir(path_test_images)
random_file = choice(test_files_list)
image = load_img(path_test_images + random_file)
image_array = img_to_array(image)

image_array = np.divide(image_array, 255.0) #normalize image

#randomly chosen scalar to add to the image, just making it sure that the resulting values are in the correct range
delta = -0.5 + np.random.random() #randomly chosen scalar
image_modified = (delta + image_array)*(delta + image_array >= 0)*(delta + image_array <= 1.0) + (delta + image_array > 1.0)

#save modified image
plt.imshow(array_to_img(image_modified))
plt.axis('off')
plt.tight_layout(pad = 0)
plt.savefig(path_modified_images + random_file, bbox_inches = 'tight', pad_inches = 0)
plt.close()

#use modified image as input for our CNN to predict delta
image_for_model = np.expand_dims(resize(image_modified, (128, 128)), axis=0)
pred = model.model.predict(image_for_model)
delta_pred = pred[0][0][0][0] #delta prediction

#load and normalize the modified image
image_to_fix = load_img(path_modified_images + random_file)
image_to_fix = img_to_array(image_to_fix)
image_to_fix = np.divide(image_to_fix, 255.0)

#fix modified image by substracting the predicted scalar delta_pred
image_fixed = (-delta_pred + image_to_fix)*(-delta_pred + image_to_fix >= 0)*(-delta_pred + image_to_fix <= 1.0) + (-delta_pred + image_to_fix > 1.0)#, delta = delta)

#save fixed image
plt.imshow(array_to_img(image_fixed))# -  image_array))
plt.axis('off')
plt.tight_layout(pad = 0)
plt.savefig(path_fixed_images + random_file, bbox_inches = 'tight', pad_inches = 0)
plt.close()

#print true and predicted values for delta
print(f'True delta: {delta}, Predicted delta: {delta_pred}')


#show and save all three versions of the image
fig, axs = plt.subplots(1, 3, figsize=(12, 4))

axs[0].imshow(array_to_img(image_array))
axs[0].set_title('Original', fontsize = 10)
axs[0].axis('off')

axs[1].imshow(array_to_img(image_modified))
axs[1].set_title('Modified', fontsize = 10)
axs[1].axis('off')

axs[2].imshow(array_to_img(image_fixed))
axs[2].set_title('Fixed', fontsize = 10)
axs[2].axis('off')

fig.suptitle(f'True delta: {delta}, Predicted delta: {delta_pred}', fontsize=16)

plt.subplots_adjust(wspace = 0.5)#, hspace = 0.35)
plt.tight_layout(pad = 1)        
plt.savefig('Random_test_image.jpg')#, bbox_inches = 'tight', pad_inches = 0)
plt.show()
