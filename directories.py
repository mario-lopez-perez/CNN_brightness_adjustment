#This script just contains a couple of trivial but useful functions to get paths and directories.
import os 

#Given category 'cat', this function returns the correct path to the images dataset
def images_path(cat): 
    if cat == 'arch':
        path = 'images/architecure/'
    elif cat == 'ac':
        path = 'images/art and culture/'
    elif cat == 'fd':
        path = 'images/food and d rinks/'
    elif cat == 'ta':
        path = 'images/travel and adventure/'
    #elif cat == 'all':
    #    path = 'images'
    return path

#It creates a directory if it doesn't already exist
def create_if_doesnt_exist(path):
    if not os.path.exists(path):
        os.makedirs(path)
   