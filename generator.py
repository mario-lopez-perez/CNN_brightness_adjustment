#This script contains the Generator class, used to modify the correctly brightened 
#images in the original dataset to obtain a suitable training dataset of incorrectly 
#brightened images

import numpy as np
from tensorflow.keras.utils import Sequence, load_img, img_to_array
import tensorflow as tf
from random import shuffle
resize = tf.image.resize

class Generator(Sequence):
    def __init__(self,images,batch_size):
        self.image_set = images
        self.batch_size = batch_size
        self.len = int(np.ceil(len(self.image_set)/self.batch_size))

    def __len__(self):
        return int(self.len)

    def __getitem__(self, idx):
        y = []
        x = []
        current_list = self.image_set[int(idx * self.batch_size): int((idx + 1) * self.batch_size)]
        for img in current_list:
            image = load_img(img)
            image = img_to_array(image)
            image = resize(image, (128, 128)).numpy() #the images are resized, since they are not uniformly shaped
            image = np.divide(image,255.0) #normalize image
            
            #delta is the random scarlar to be added to the original image
            delta = -0.5 + np.random.random() 

            #This is done to be sure that the resulting values are in the correct (0,1) range
            image = (delta + image)*(delta + image >= 0)*(delta + image <= 1.0) + (delta + image > 1.0)#, delta = delta)
            x.append(image) #the modified image is added to the training dataset
            y.append(np.asarray([delta]).reshape(1,1,1)) #the true delta, the value to be learned by the model, is added to the set of outputs

        return np.asarray(x), np.asarray(y)

    def on_epoch_end(self):
        shuffle(self.image_set)


