#This script is the one which trains the model
from model import BRC
from generator import Generator
import glob
import os
from random import shuffle
import argparse
from matplotlib import pyplot as plt
import tensorflow
from directories import *
import argparse
#tensorflow.compat.v1.disable_eager_execution() #eager execution makes it much slower

Adam = tensorflow.keras.optimizers.Adam
ModelCheckpoint = tensorflow.keras.callbacks.ModelCheckpoint 
TensorBoard = tensorflow.keras.callbacks.TensorBoard #,EarlyStopping
resize = tensorflow.image.resize

# Get the model
model = BRC()
#model.model.summary()


parser_category = argparse.ArgumentParser() #the cat argument defines the category of images
parser_category.add_argument('-cat', '--category', type = str, required = True, help = "This script trains the CNN model to automatically fix brightnes for images in the google scrapped images dataset. These images are divided in four categories, please select one of them: 'arch' for architecture, 'a&t' for art and culture, 'fd' for food and drink, 'ta' for travel and andventure") 
args = parser_category.parse_args()

cat = args.category
path = images_path(cat) #see the directories.py file, it is just a function to create a path depending on the chosen category
files_list = os.listdir(path)
shuffle(files_list)

#The next block is about splitting the dataset: 80% for training, 15% for validation and 5% for testing in the prediction stage. A new folder for the testing images is created if it doesn't already exist. 
path_test_images = path + 'test_images/'
if not os.path.exists(path_test_images):
    os.makedirs(path_test_images)
    split_1 = int(0.8*len(files_list))
    split_2 = int(0.95*len(files_list))
    train_images_list = [path + file for file in files_list[0:split_1]]
    valid_images_list = [path + file for file in files_list[split_1: split_2]]
    train = Generator(train_images_list,8)
    valid = Generator(valid_images_list, 8).__getitem__(5)
    for file in files_list[split_2: ]:
        os.rename(path + file, path_test_images + file)
else:
    split = int(0.8*len(files_list)/0.95)
    train_images_list = [path + file for file in files_list[0:split] if file != 'test_images/']
    valid_images_list = [path + file for file in files_list[split: ]if file != 'test_images/']
    train = Generator(train_images_list,8)
    valid = Generator(valid_images_list, 8).__getitem__(5)


# Compile model then run
optimizer = Adam(learning_rate = 0.002)
model.model.compile(loss = 'mean_squared_error',optimizer = optimizer)#, metrics = ['accuracy'])


# Define Callbacks (save model, validation etc)
ckpt = ModelCheckpoint(cat +'.h5',save_best_only=True,mode = 'min')
tsb = TensorBoard(log_dir=os.path.join( os.getcwd(),'logs' ),write_graph=True,histogram_freq=1)
#early_stop_cb = EarlyStopping(monitor='val_loss',
#                           min_delta=0.001,
#                           patience=3,
#                           mode='min',
#                           verbose=1)
callback = [ckpt,tsb]#,early_stop_cb]

# Train the model
r = model.model.fit(train,
                steps_per_epoch = 150,
                epochs = 100,
                verbose = 1,
                validation_data = valid,
                callbacks = callback)

#plot the loss evolution through epochs 
plt.plot(r.history['loss'], label = 'loss')
plt.plot(r.history['val_loss'], label = 'validation loss')
plt.legend()
plt.savefig(cat + '_loss.jpg')
plt.close()
