# Brightness Adjustment

This project is about automatically adjusting brightness in images from the [google scrapped image datatset](https://www.kaggle.com/code/shruthimshruthim/google-scrapped-image/data) using a simple Convolutional Neural Network. It is based on [an existing repository](https://github.com/dht7166/Brightness_Contrast). While results are not astonishing, it is a first working model to build on.

The standard way of changing brightness is just to add a scalar δ to the image, so if we are given a dataset of already adjusted images, we can sum random scalars to them to modify the images and ask our CNN to learn those scalars.

The dataset is divided in four categories: 'architecture', 'art and culture', 'food and drinks' and 'travel and adventure', and the CNN works in each of them separately, as their statistical properties are way too different. So, if you want to test the model you must choose one of these categories: 'arch', 'ac', 'fd' or 'ta'. If you chose, lets say, 'arch', then you can run the prediction script with the selected category like this: 

```python3 prediction.py -cat arch```

This will load the pre-trained weights of the CNN (all four of them have been already calculated and included in the repository), randomly pick one of the test images (already splitted in this repository when the training was being done), modify it by adding to it a uniformly random scalar δ between -0.5 and 0.5 (a reasonable range for modifyng these images), predict this scalar by evaluating the CNN on the modified image and fix the modified image using the predicted value for δ, showing the three images at the end and printing the real and predicted values for δ. All the modified and fixed images produced are stored in the corresponding categorical images folders.

Here is an example of an image randomly picked, modified and fixed by the 'prediction.py' file:
![a1](Random_test_image.jpg) 

As already stated, the pre-trained weights are included in this repository, and if you just want to see how the model works, it won't be necesssary to train again the models. However, if you insist in repeating the training process or you want to modify some hyper-parameters or maybe you want to train it in another dataset, you can train the model and store the obtained weights running this line of code (for architecture category):

```python3 training.py -cat arch```

However, trivial as it seems, the problem is as complicated as one allows it to be. See for example [this paper](https://arxiv.org/abs/1412.7725) by Zhicheng Yan et al., as well as [the corresponding GitHub repository](https://github.com/stephenyan1231/dl-image-enhance), which deal with much more complicated algorithms to take into consideration image local content and semantics.
